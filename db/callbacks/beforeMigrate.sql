/**
 * Permissoes para conectar no schema
 */
	/*
		CRIAR USUARIO:
		CREATE USER pdca_TEST IDENTIFIED BY flyway;
	*/
	GRANT CREATE SESSION TO pdca_TEST WITH ADMIN OPTION;
	GRANT ALL PRIVILEGES TO pdca_TEST;
	GRANT DBA TO pdca_TEST;
	
	/**
	 * Habilita semantica de tamanho para caracteres UTF-8
	 * Resumindo: 1 char = 4 bytes
	 */
	ALTER session SET nls_length_semantics=CHAR;
	
	/**
	 * Habilita EBR (Edition-Based Redefinition) para usuario
	*/
	ALTER USER pdca_TEST ENABLE EDITIONS;  


