-- Create table
create table USUARIOS
(
  COD_USUARIO        VARCHAR2(15) not null,
  COD_EMPRESA_PADRAO VARCHAR2(3) not null,
  NOM_FUNCIONARIO    VARCHAR2(400) not null,
  NUM_TELEFONE       VARCHAR2(20),
  NUM_RAMAL          VARCHAR2(5),
  NUM_FAX            VARCHAR2(30),
  E_MAIL             VARCHAR2(400),
  QTD_MAX_USUARIOS   NUMBER(5),
  IND_ADMLOG         VARCHAR2(1),
  GRUPO_PADRAO       VARCHAR2(8),
  TP_USUARIO         VARCHAR2(1) default 'U',
  CRIADO_POR         VARCHAR2(15),
  DATA_CRIACAO       DATE,
  ATUALIZADO_POR     VARCHAR2(15),
  DATA_ATUALIZACAO   DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table USUARIOS
  add constraint PK_USUARIOS primary key (COD_USUARIO);
  
-- Create table
create table MDB_ACTIVE_DIRECTORY_VM
(
  SAMACCOUNTNAME     VARCHAR2(4000),
  CN                 VARCHAR2(4000),
  TITLE              VARCHAR2(4000),
  DEPARTMENT         VARCHAR2(4000),
  COMPANY            VARCHAR2(4000),
  MAIL               VARCHAR2(4000),
  USERACCOUNTCONTROL VARCHAR2(4000),
  DISPLAYNAME        VARCHAR2(4000),
  OFFICENAME         VARCHAR2(4000),
  TELEPHONENUMBER    VARCHAR2(4000),
  FACSIMILETELEPHONE VARCHAR2(4000),
  IPPHONE            VARCHAR2(4000),
  HOMEPHONE          VARCHAR2(4000),
  MOBILE             VARCHAR2(4000),
  DISTINGUISHEDNAME  VARCHAR2(4000)
);

-- Create table
create table EMPRESA
(
  COD_EMPRESA     VARCHAR2(3) not null,
  DEN_EMPRESA     VARCHAR2(30) not null,
  DEN_REDUZ       VARCHAR2(15) not null,
  OU_EMPRESA      NUMBER(15) not null,
  ID_EMPRESA      NUMBER(15),
  COD_ORGANIZACAO VARCHAR2(3)
);

-- Create table
create table MDB_ACS_ACESSOS
(
  COD_USUARIO           VARCHAR2(15) not null,
  COD_GRUPO_CONCATENADO VARCHAR2(250) not null,
  COD_GRUPO             VARCHAR2(150) not null,
  DATA_VALIDADE         DATE,
  DATA_ATUALIZACAO      DATE,
  CRIADO_POR            VARCHAR2(15),
  DATA_CRIACAO          DATE,
  ATUALIZADO_POR        VARCHAR2(15)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_ACESSOS
  add constraint MDB_ACS_ACESSOS_PK primary key (COD_USUARIO, COD_GRUPO_CONCATENADO, COD_GRUPO);



-- Create table
create table MDB_ACS_GRUPO_X_ABRANG
(
  COD_GRUPO_CONCATENADO    VARCHAR2(250) not null,
  COD_GRUPO                VARCHAR2(150) not null,
  COD_TP_ABRANGENCIA       VARCHAR2(30),
  DESC_PREFIXO_ABRANGENCIA VARCHAR2(150),
  COD_OU_EMPRESA           NUMBER(15),
  ORG_ID                   NUMBER(15),
  BRANCH_CODE              VARCHAR2(3),
  ORGANIZATION_ID          NUMBER(15),
  FLAG_ABRANG_ESCOLHIDA    CHAR(1),
  FLAG_ACESSO_CRITICO      CHAR(1) default 'N' not null,
  COD_APROVADOR_CRITICO    VARCHAR2(15),
  COD_APROVADOR_INFO       VARCHAR2(15) not null,
  DATA_ATUALIZACAO         DATE,
  CRIADO_POR               VARCHAR2(15),
  DATA_CRIACAO             DATE,
  ATUALIZADO_POR           VARCHAR2(15)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_GRUPO_X_ABRANG
  add constraint MDB_ACS_GRUPO_X_ABRANG_PK primary key (COD_GRUPO, COD_GRUPO_CONCATENADO);
  
  
CREATE OR REPLACE VIEW SCA_GRP_EMP_USU AS
SELECT magxa.branch_code cod_empresa
     , maa.cod_grupo
     , maa.cod_usuario
     , maa.data_validade dat_validade
  FROM mdb_acs_acessos        maa
     , mdb_acs_grupo_x_abrang magxa
 WHERE magxa.cod_grupo = maa.cod_grupo
   AND magxa.cod_grupo_concatenado = maa.cod_grupo_concatenado
   AND magxa.flag_abrang_escolhida = 'F';

-- Create table
create table SCA_FUNC_GRP_NIVEL
(
  COD_GRUPO        VARCHAR2(150) not null,
  NUM_PROGRAMA     VARCHAR2(30) not null,
  NUM_NIVEL        CHAR(1),
  COD_OPERADOR     VARCHAR2(8),
  DAT_OPERACAO     DATE,
  DATA_ATUALIZACAO DATE,
  CRIADO_POR       VARCHAR2(15),
  DATA_CRIACAO     DATE,
  ATUALIZADO_POR   VARCHAR2(15)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table SCA_FUNC_GRP_NIVEL
  add constraint PK_SCA_FUNC_GRP_NI primary key (COD_GRUPO, NUM_PROGRAMA);
   
-- Create table
create table SCA_FUNCAO
(
  NUM_PROGRAMA     VARCHAR2(30) not null,
  DEN_PROGRAMA     VARCHAR2(100) not null,
  NUM_NIVEL_FIXO   CHAR(1),
  COD_SISTEMA      VARCHAR2(8),
  HORA_INI_EXEC    DATE,
  HORA_FIM_EXEC    DATE,
  CRIADO_POR       VARCHAR2(15),
  DATA_CRIACAO     DATE,
  ATUALIZADO_POR   VARCHAR2(15),
  DATA_ATUALIZACAO DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table SCA_FUNCAO
  add constraint PK_SCA_FUNCAO primary key (NUM_PROGRAMA);
  
-- Create table
create table MDB_ACS_MENU
(
  MENU_ID          NUMBER(15) not null,
  COD_SISTEMA      VARCHAR2(8) not null,
  NUM_PROGRAMA     VARCHAR2(30),
  MENU_PAI         NUMBER(15),
  MENU_DESCRICAO   VARCHAR2(30) not null,
  TIPO_URL         NUMBER(1) not null,
  ENDERECO_URL     VARCHAR2(300),
  VISIVEL          CHAR(1) not null,
  ORDEM            NUMBER(10) default 0 not null,
  CRIADO_POR       VARCHAR2(15),
  DATA_CRIACAO     DATE,
  ATUALIZADO_POR   VARCHAR2(15),
  DATA_ATUALIZACAO DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_MENU
  add constraint PK_MDB_ACS_MENU primary key (MENU_ID, COD_SISTEMA);  
  
-- Create table
create table MDB_ACS_ACOES
(
  COD_SISTEMA      VARCHAR2(8) not null,
  ACAO_DESCRICAO   VARCHAR2(80) not null,
  ACAO_COD         VARCHAR2(20) not null,
  TIPO_ACAO_COD    NUMBER(15) not null,
  NUM_PROGRAMA     VARCHAR2(30),
  GLOBAL           CHAR(1) not null,
  ACESSO_LIVRE     CHAR(1),
  CRIADO_POR       VARCHAR2(15),
  DATA_CRIACAO     DATE,
  ATUALIZADO_POR   VARCHAR2(15),
  DATA_ATUALIZACAO DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_ACOES
  add constraint PK_MDB_ACS_ACOES primary key (COD_SISTEMA, ACAO_COD);   
  
  
-- Create table
create table MDB_ACS_USUARIO_ACESSO
(
  COD_TOKEN    VARCHAR2(300) not null,
  COD_USUARIO  VARCHAR2(10) not null,
  DATA_CRIACAO DATE not null,
  ST_ACESSO    VARCHAR2(1) not null
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_USUARIO_ACESSO
  add constraint MDB_USUARIO_ACESSO_PK primary key (COD_TOKEN);
alter table MDB_ACS_USUARIO_ACESSO
  add constraint MDB_ACS_USUARIO_ACESSO_U1 unique (COD_USUARIO, DATA_CRIACAO);

  
-- Create table
create table MDB_ACS_MAQ_ACESSO
(
  COD_TOKEN       VARCHAR2(300) not null,
  COD_SISTEMA     VARCHAR2(20) not null,
  DATA_ACESSO     DATE not null,
  VAL_ENDERECO_IP VARCHAR2(100) not null,
  DESC_MAQUINA    VARCHAR2(100) not null,
  ST_ACESSO       VARCHAR2(1) not null
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDB_ACS_MAQ_ACESSO
  add constraint MDB_ACS_MAQ_ACESSO_PK primary key (COD_TOKEN, COD_SISTEMA, DATA_ACESSO, ST_ACESSO, VAL_ENDERECO_IP);
  
-- Create table
create table SCA_GRUPO
(
  COD_GRUPO                VARCHAR2(150) not null,
  DEN_GRUPO                VARCHAR2(2000) not null,
  FLAG_POS_SETUP           CHAR(1) default 'N' not null,
  VAL_OBS_POS_SETUP        VARCHAR2(4000),
  COD_OPERADOR             VARCHAR2(8),
  DAT_OPERACAO             DATE,
  TRABALHA_EM_CONTINGENCIA CHAR(1) default 'N',
  COD_SISTEMA              VARCHAR2(8),
  DATA_ATUALIZACAO         DATE,
  CRIADO_POR               VARCHAR2(15),
  DATA_CRIACAO             DATE,
  ATUALIZADO_POR           VARCHAR2(15)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table SCA_GRUPO
  add constraint PK_SCA_GRUPO primary key (COD_GRUPO);
  
  
create table FND_USER
(
	 WEB_PASSWORD					VARCHAR2(240)         
	,SUPPLIER_ID					NUMBER(22)            
	,CUSTOMER_ID					NUMBER(22)            
	,FAX							VARCHAR2(80)          not null
	,EMAIL_ADDRESS					VARCHAR2(240)         not null
	,EMPLOYEE_ID					NUMBER(22)            not null
	,PASSWORD_LIFESPAN_DAYS			NUMBER(22)            not null
	,PASSWORD_LIFESPAN_ACCESSES		NUMBER(22)            not null
	,PASSWORD_ACCESSES_LEFT			NUMBER(22)            not null
	,PASSWORD_DATE					DATE                  
	,LAST_LOGON_DATE				DATE                  not null
	,DESCRIPTION					VARCHAR2(240)         not null
	,END_DATE						DATE                  not null
	,START_DATE						DATE                  not null
	,SESSION_NUMBER					NUMBER(22)            
	,ENCRYPTED_USER_PASSWORD		VARCHAR2(100)         
	,ENCRYPTED_FOUNDATION_PASSWORD	VARCHAR2(100)         
	,LAST_UPDATE_LOGIN				NUMBER(22)            
	,CREATED_BY						NUMBER(22)            
	,LAST_UPDATED_BY				NUMBER(22)            
	,LAST_UPDATE_DATE				DATE                  
	,USER_NAME						VARCHAR2(100)         
	,USER_ID						NUMBER(22)            
	,CREATION_DATE					DATE                  
	,USER_GUID						RAW(16)               
	,PERSON_PARTY_ID				NUMBER(22)            
	,GCN_CODE_COMBINATION_ID		NUMBER(22)            
);
