package br.com.mdias.listeners;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

public class PreventAnyUpdate {

    @PrePersist
    void onPrePersist(Object o) {
        throw new RuntimeException("Objeto somente leitura");
    }

    @PreUpdate
    void onPreUpdate(Object o) {
        throw new RuntimeException("Objeto somente leitura");
    }

    @PreRemove
    void onPreRemove(Object o) {
        throw new RuntimeException("Objeto somente leitura");
    }
}