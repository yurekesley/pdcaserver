package br.com.mdias.comparator;

import java.util.Comparator;

import br.com.mdias.pdca.entidade.Estratificacao;

public class PrioridadeSorter implements Comparator<Estratificacao>{

	@Override
	public int compare(Estratificacao e1, Estratificacao e2) {
		return e1.getPrioridade().compareTo(e2.getPrioridade());
	}

}
