package br.com.mdias.comparator;

import java.util.Comparator;

import br.com.mdias.pdca.entidade.Estratificacao;

public class QuantidadeSorter implements Comparator<Estratificacao>{

	@Override
	public int compare(Estratificacao e1, Estratificacao e2) {
		return e2.getQuantidade().intValue() - e1.getQuantidade().intValue();
	}

}
