package br.com.mdias.pdca;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.mdias.pdca.repositorio.AcaoRepositorio;

@Component
public class PDCAScheduling {
	
	@Autowired
	AcaoRepositorio acaoRepositorio;
	
	@Scheduled(cron = "0 0 0 1/1 * ?")
	public void atualizarAcoesAtrasadas() {
		acaoRepositorio.atualizarAtrasos(new Date());
	}

}
