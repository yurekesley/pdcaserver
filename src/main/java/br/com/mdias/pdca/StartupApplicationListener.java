package br.com.mdias.pdca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

	public static boolean naoExecutado = true;

	@Autowired
	PDCAScheduling scheduling;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (StartupApplicationListener.naoExecutado) {
			scheduling.atualizarAcoesAtrasadas();
			StartupApplicationListener.naoExecutado = false;
		}
	}

}
