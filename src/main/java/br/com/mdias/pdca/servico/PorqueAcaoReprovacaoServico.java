package br.com.mdias.pdca.servico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdias.frmk_consulta.repositorio.ConsultaRepositorio;
import br.com.mdias.pdca.entidade.dto.ReprovacaoDTO;

@Service
public class PorqueAcaoReprovacaoServico {
	
	@Autowired
	private ConsultaRepositorio consulta;
	
	
	public List<ReprovacaoDTO> porPDCA(Long idPDCA) {
		Map<String, Object> filtros = new HashMap<>();
		filtros.put("idPDCA", idPDCA);
		return consulta.listagem("REPROVACOES-POR-PDCA", filtros);
	}
}
