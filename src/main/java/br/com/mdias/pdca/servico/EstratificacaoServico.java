package br.com.mdias.pdca.servico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.comparator.PrioridadeSorter;
import br.com.mdias.comparator.QuantidadeSorter;
import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.frmk_utilitario.infraestrutura.consulta.IConsultaBuilder;
import br.com.mdias.pdca.entidade.Estratificacao;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.dto.EstratificacaoDTO;
import br.com.mdias.pdca.repositorio.EstratificacaoRepositorio;

@Service
public class EstratificacaoServico extends GenericoCrudServico<Estratificacao, Long> {

	@Autowired
	private CausaRaizServico causaRaizServico;

	@Autowired
	private EstratificacaoRepositorio repositorio;

	@Transactional
	public List<Estratificacao> excluir(Estratificacao estratificacao) {
		estratificacao = this.consultarPor(estratificacao.getId());

		if (estratificacao.getFilhos() != null && estratificacao.getFilhos().size() > 0) {
			for (Estratificacao estTemp : estratificacao.getFilhos()) {
				this.excluir(estTemp);
			}
		}

		causaRaizServico.excluirPorEstratificacao(estratificacao.getId());
		super.deletar(estratificacao);

		ArrayList<Estratificacao> estratificacoes;
		if (estratificacao.getPai() != null && estratificacao.getPai().getId() != null) {
			estratificacoes = (ArrayList<Estratificacao>) filhos(estratificacao.getPai().getId());
		} else {
			estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(estratificacao.getPdca().getId());
		}
		ordenarPorQuantidade(estratificacoes);
		return estratificacoes;
	}

	@Transactional
	public void excluirTodos(Long id) {
		Estratificacao estratificacao = repositorio.findOne(id);
		boolean possuiPai = estratificacao.getPai() != null && estratificacao.getPai().getId() != null;
		if (possuiPai) {
			List<Estratificacao> exclusoes = listarPorPDCAPorPai(estratificacao.getPdca().getId(),
					estratificacao.getPai().getId());
			for (Estratificacao item : exclusoes) {
				this.excluir(item);
			}
			return;
		}

		List<Estratificacao> exclusoes = listarPorPDCA(estratificacao.getPdca().getId());
		for (Estratificacao item : exclusoes) {
			this.excluir(item);
		}
	}

	public List<Estratificacao> adicionar(Estratificacao estratificacao) {
		ArrayList<Estratificacao> estratificacoes;
		if (estratificacao.getPai() != null && estratificacao.getPai().getId() != null) {
			estratificacoes = (ArrayList<Estratificacao>) filhos(estratificacao.getPai().getId());
		} else {
			estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(estratificacao.getPdca().getId());
		}

		estratificacoes.add(estratificacao);
		ordenarPorQuantidade(estratificacoes);
		this.salvar(estratificacoes);
		return estratificacoes;
	}

	@Transactional
	public List<Estratificacao> aumentarPrioridade(Estratificacao estratificacao) {
		ArrayList<Estratificacao> estratificacoes;
		if (estratificacao.getPai() != null && estratificacao.getPai().getId() != null) {
			estratificacoes = (ArrayList<Estratificacao>) filhos(estratificacao.getPai().getId());
		} else {
			estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(estratificacao.getPdca().getId());
		}

		if (estratificacoes == null || estratificacoes.size() <= 1) {
			return estratificacoes;
		}
		ordenarPorPrioridade(estratificacoes);
		boolean ehItemDeMaiorPrioridade = estratificacoes.get(0).equals(estratificacao);
		if (ehItemDeMaiorPrioridade) {
			return estratificacoes;
		}
		int indiceDoAlvoDaTroca = 0;
		for (Estratificacao item : estratificacoes) {
			if (item.getId().equals(estratificacao.getId())) {
				break;
			}
			indiceDoAlvoDaTroca++;
		}
		Integer prioridadeDoAlvoDaTroca = estratificacoes.get(indiceDoAlvoDaTroca - 1).getPrioridade();
		estratificacoes.get(indiceDoAlvoDaTroca - 1)
				.setPrioridade(estratificacoes.get(indiceDoAlvoDaTroca).getPrioridade());
		estratificacoes.get(indiceDoAlvoDaTroca).setPrioridade(prioridadeDoAlvoDaTroca);
		ordenarPorPrioridade(estratificacoes);
		return estratificacoes;
	}

	@Transactional
	public List<Estratificacao> diminuirPrioridade(Estratificacao estratificacao) {
		ArrayList<Estratificacao> estratificacoes;
		if (estratificacao.getPai() != null && estratificacao.getPai().getId() != null) {
			estratificacoes = (ArrayList<Estratificacao>) filhos(estratificacao.getPai().getId());
		} else {
			estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(estratificacao.getPdca().getId());
		}

		if (estratificacoes == null || estratificacoes.size() <= 1) {
			return estratificacoes;
		}
		ordenarPorPrioridade(estratificacoes);
		boolean ehItemDeMenorPrioridade = estratificacoes.get(estratificacoes.size() - 1).equals(estratificacao);
		if (ehItemDeMenorPrioridade) {
			return estratificacoes;
		}
		int indiceDoAlvoDaTroca = 0;
		for (Estratificacao item : estratificacoes) {
			if (item.getId().equals(estratificacao.getId())) {
				break;
			}
			indiceDoAlvoDaTroca++;
		}
		Integer prioridadeDoAlvoDaTroca = estratificacoes.get(indiceDoAlvoDaTroca + 1).getPrioridade();
		estratificacoes.get(indiceDoAlvoDaTroca + 1)
				.setPrioridade(estratificacoes.get(indiceDoAlvoDaTroca).getPrioridade());
		estratificacoes.get(indiceDoAlvoDaTroca).setPrioridade(prioridadeDoAlvoDaTroca);
		ordenarPorPrioridade(estratificacoes);
		return estratificacoes;
	}

	@Transactional
	public List<Estratificacao> atualizar(Estratificacao estratificacao) {
		ArrayList<Estratificacao> estratificacoes;
		if (estratificacao.getPai() != null && estratificacao.getPai().getId() != null) {
			estratificacoes = (ArrayList<Estratificacao>) filhos(estratificacao.getPai().getId());
		} else {
			estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(estratificacao.getPdca().getId());
		}
		if (estratificacoes != null) {
			for (Estratificacao estratificacaoPDCA : estratificacoes) {
				if (estratificacaoPDCA.getId().equals(estratificacao.getId())) {
					estratificacaoPDCA.setOque(estratificacao.getOque());
					estratificacaoPDCA.setQuantidade(estratificacao.getQuantidade());
					break;
				}
			}
		}
		ordenarPorQuantidade(estratificacoes);
		return estratificacoes;
	}

	public List<Estratificacao> listarPorPDCA(Long idPDCA) {
		return ((EstratificacaoRepositorio) getRepositorio()).primeirasEstratificacoes(idPDCA);
	}

	public List<Estratificacao> listarPorPDCAPorPai(Long idPDCA, Long idPai) {
		return ((EstratificacaoRepositorio) getRepositorio()).listarPorPai(idPDCA, idPai);
	}

	public void ordenarPorQuantidade(ArrayList<Estratificacao> estratificacoes) {
		Collections.sort(estratificacoes, new QuantidadeSorter());
		Integer prioridade = 0;
		for (Estratificacao estratificacao : estratificacoes) {
			estratificacao.setPrioridade(prioridade);
			prioridade++;
		}
	}

	public void ordenarPorPrioridade(ArrayList<Estratificacao> estratificacoes) {
		Collections.sort(estratificacoes, new PrioridadeSorter());
		Integer prioridade = 0;
		for (Estratificacao estratificacao : estratificacoes) {
			estratificacao.setPrioridade(prioridade);
			prioridade++;
		}
	}

	public List<Estratificacao> filhos(Long id) {
		Estratificacao filtros = new Estratificacao();
		filtros.setPai(new Estratificacao());
		filtros.getPai().setId(id);
		ArrayList<Estratificacao> filhos = (ArrayList<Estratificacao>) this.consultarPor(filtros);
		this.ordenarPorPrioridade(filhos);
		return filhos;
	}

	public List<Estratificacao> pais(Long id, List<Estratificacao> estratificacoes) {
		estratificacoes = estratificacoes == null ? new ArrayList<Estratificacao>() : estratificacoes;
		Estratificacao filho = ((EstratificacaoRepositorio) getRepositorio()).findOne(id);
		estratificacoes.add(filho);
		if (filho.getPai() == null || filho.getPai().getId() == null) {
			Collections.reverse(estratificacoes);
			estratificacoes.remove(estratificacoes.size() - 1);
			return estratificacoes;
		} else {
			return pais(filho.getPai().getId(), estratificacoes);
		}
	}

	public List<EstratificacaoDTO> visualizarDTO(Long idPDCA) {
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idPdca", idPDCA);

		IConsultaBuilder consultaBuilder = this.getConsultaRepositorio().getConsultaBuilder();
		List<EstratificacaoDTO> estratificacoes = this.getConsultaRepositorio()
				.listagem("ESTRATIFICACOES_POR_ESTRATIFICACAO", filtro);
		consultaBuilder.consultarData(estratificacoes, filtro);

		return estratificacoes;
	}

	@Transactional
	public void excluirPorPDCA(PDCA pdca) {
		ArrayList<Estratificacao> estratificacoes = (ArrayList<Estratificacao>) listarPorPDCA(pdca.getId());
		for (Estratificacao estratificacao : estratificacoes) {
			this.excluir(estratificacao);
		}
	}

}
