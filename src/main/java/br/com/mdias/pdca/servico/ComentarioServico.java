package br.com.mdias.pdca.servico;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.Comentario;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.dto.AcaoDTO;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import br.com.mdias.pdca.entidade.enums.StatusPDCA;

@Service
public class ComentarioServico extends GenericoCrudServico<Comentario, Long> {

	@Autowired
	private AcaoServico acaoServico;
	
	@Autowired
	private PDCAServico pdcaServico;

	@Override
	@Transactional
	public void salvar(Comentario comentario) {
		getRepositorioGenerico().save(comentario);
		Acao acao = acaoServico.consultarPor(comentario.getAcao().getId());
		if (comentario.getStatusAcao() != acao.getStatusAcao()) {
			if(comentario.getStatusAcao().equals(StatusAcao.CONCLUIDA)) {
				acao.setConclusao(new Date());
			}
			acao.setStatusAcao(comentario.getStatusAcao());
			acaoServico.salvarFlush(acao);
			atualizarStatusDosPDCAs();
		}
	}

	private void atualizarStatusDosPDCAs() {
		PDCA filtro = new PDCA();
		filtro.setStatus(StatusPDCA.EM_EXECUCAO);
		List<PDCA> pdcas = this.pdcaServico.consultarPor(filtro);
		
		for (PDCA pdca : pdcas) {
			if(acoesConcluidas(pdca.getId())) {
				pdca.setStatus(StatusPDCA.AGUARDANDO_PARECER);
				this.pdcaServico.salvar(pdca);
			}
		}
		
	}

	private boolean acoesConcluidas(Long idPDCA) {
		Map<String, Object> filtros = new HashMap<>();
		filtros.put("idPDCA", idPDCA);
		List<AcaoDTO> acoes = this.getConsultaRepositorio().listagem("ACOES-POR-PDCA", filtros);
		for (AcaoDTO acao : acoes) {
			if (!StatusAcao.CONCLUIDA.name().equals(acao.getStatusAcao())) {
				return false;
			}
		}
		return true;
	}
}
