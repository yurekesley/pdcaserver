package br.com.mdias.pdca.servico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.Porque;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.PorqueAcaoParecer;
import br.com.mdias.pdca.entidade.PorqueAcaoReprovacao;
import br.com.mdias.pdca.entidade.dto.PorqueAcaoDTO;
import br.com.mdias.pdca.entidade.enums.SimOuNao;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import br.com.mdias.pdca.entidade.enums.StatusAtrasoAcao;
import br.com.mdias.pdca.repositorio.PorqueAcaoParecerRepositorio;
import br.com.mdias.pdca.repositorio.PorqueAcaoReprovacaoRepositorio;
import br.com.mdias.pdca.repositorio.PorqueAcoesRepositorio;

@Service
public class PorqueAcaoServico extends GenericoCrudServico<PorqueAcao, Long> {

	@Autowired
	private AcaoServico acaoServico;
		
	@Autowired
	private PorqueAcoesRepositorio repositorio;
	
	@Autowired PDCAServico pdcaServico;
	
	@Autowired
	private PorqueAcaoReprovacaoRepositorio porqueAcaoReprovacaoRepositorio;
	
	@Autowired 
	private PorqueAcaoParecerRepositorio porqueAcaoParecerRepositorio;

	@Override
	@Transactional
	public void salvar(PorqueAcao porqueAcao) {
		if (porqueAcao.getAcao().getId() == null) {
			porqueAcao.getAcao().setStatusAcao(StatusAcao.EM_CADASTRO);
			porqueAcao.getAcao().setStatusAtraso(StatusAtrasoAcao.N);
			porqueAcao.getAcao().setOrigem(porqueAcao.getPorque().getCausaRaiz().getEstratificacao().getPdca());
		}
		acaoServico.salvar(porqueAcao.getAcao());
		super.salvar(porqueAcao);
	}

	@Override
	@Transactional
	public void deletar(PorqueAcao porqueAcao) {
		this.deletar(porqueAcao.getId());
	}

	@Override
	@Transactional
	public void deletar(Long id) {
		PorqueAcao entidade = (PorqueAcao) getRepositorioGenerico().findOne(id);
		entidade = getRepositorio().save(entidade);
		getRepositorioGenerico().delete(entidade);
		limparAcoesSoltar(entidade.getAcao().getId());
	}

	private void limparAcoesSoltar(Long idAcao) {
		PorqueAcao filtro = new PorqueAcao();
		filtro.setAcao(new Acao());
		filtro.getAcao().setId(idAcao);
		List<PorqueAcao> acoes = consultarPor(filtro);
		if (acoes == null || acoes.size() == 0) {
			acaoServico.deletar(idAcao);
		}
	}
	
	
	public List<PorqueAcao> listarPorPDCA(Long idPDCA ) {
		return this.repositorio.listaPorqueAcaoPorPDCA(idPDCA);
	}
	
	
	public PorqueAcao vincular(PorqueAcao porqueAcao) {
		super.salvar(porqueAcao);
		return porqueAcao;
	}

	public List<PorqueAcao> acoesPorPDCAStatusAcao(PorqueAcaoDTO dto) {
		return this.repositorio.acoesPorPDCAStatusAcao(dto.getIdPDCA(), dto.getStatusAcao());
	}
	
	public void alterarStatusDasAcoesPorPDCA(Long idPDCA, StatusAcao status) {
		List<PorqueAcao> porqueAcoes = this.listarPorPDCA(idPDCA);
		for (PorqueAcao porqueAcao : porqueAcoes) {
			alterarStatusDasAcoes(porqueAcao,status);
		}
	}
	
	public void alterarStatusDasAcoes(PorqueAcao porqueAcao, StatusAcao status) {
		Acao acao = porqueAcao.getAcao();
		Long pdcaSendoAvaliado = getPDCADoPoqueAcao(porqueAcao).getId();
		Long pdcaQueOriginouAAcao = acao.getOrigem().getId();
		if (pdcaQueOriginouAAcao.equals(pdcaSendoAvaliado)) {
			acao.setStatusAcao(status);					
		}
	}
	
	private PDCA getPDCADoPoqueAcao(PorqueAcao porqueAcao) {
		if(
					porqueAcao.getPorque() == null 
				|| porqueAcao.getPorque().getCausaRaiz() == null
				|| porqueAcao.getPorque().getCausaRaiz().getEstratificacao() == null 
				|| porqueAcao.getPorque().getCausaRaiz().getEstratificacao().getPdca() == null
				|| porqueAcao.getPorque().getCausaRaiz().getEstratificacao().getPdca().getId() == null
		) {
			return this.consultarPor(porqueAcao.getId()).getPorque().getCausaRaiz().getEstratificacao().getPdca();
		}
		
		return porqueAcao.getPorque().getCausaRaiz().getEstratificacao().getPdca();
	}
	
	private Porque getPorque(PorqueAcao porqueAcao) {
		if(porqueAcao.getPorque() == null || porqueAcao.getPorque().getId() == null) {
			return this.consultarPor(porqueAcao.getId()).getPorque();
		}
		return porqueAcao.getPorque();
	}
	
	@Transactional
	public void aprovar(List<PorqueAcao> porqueAcoes) {
		for (PorqueAcao pqAcao : porqueAcoes) {
			PorqueAcao porqueAcao = this.consultarPor(pqAcao.getId());
			porqueAcao.setAprovado(SimOuNao.S);
		}
	}
	
	
	@Transactional
	public void reprovar(List<PorqueAcao> porqueAcoes) {
		if(possuiAlgumaReprovacao(porqueAcoes)) {
			for (PorqueAcao pqAcao : porqueAcoes) {
				pqAcao.setPorque(getPorque(pqAcao));
				this.repositorio.save(pqAcao);	
			}		
		}
	}
	
	@Transactional
	public void anularAprovacao(Long idPDCA) {
		List<PorqueAcao> porqueAcoes = listarPorPDCA(idPDCA);
		for (PorqueAcao pqAcao : porqueAcoes) {
			PorqueAcao porqueAcao = this.consultarPor(pqAcao.getId());
			porqueAcao.setAprovado(null);
		}
	}

	
	
	
	private void addNovaReprovacao(PorqueAcao pqAcao) {
		List<PorqueAcaoReprovacao> reprovacoes = this.buscarReprovacoes(pqAcao); 
		reprovacoes.addAll(pqAcao.getReprovacoes());
		pqAcao.setReprovacoes(reprovacoes);	
	}
	
	private boolean possuiAlgumaReprovacao(List<PorqueAcao> porqueAcoes) {
		for (PorqueAcao pqAcao : porqueAcoes) {
			if(pqAcao.getAprovado().equals(SimOuNao.N)) {
				return true;
			}
		}
		return false;
	}
	
	private List<PorqueAcaoReprovacao> buscarReprovacoes(PorqueAcao pqAcao) {
		PorqueAcao porqueAcaoDB = this.repositorio.findOne(pqAcao.getId());
		return porqueAcaoDB.getReprovacoes();
	}
	
	public void salvarParecer(PorqueAcaoParecer parecer) {		
		this.porqueAcaoParecerRepositorio.save(parecer);
	}
}
