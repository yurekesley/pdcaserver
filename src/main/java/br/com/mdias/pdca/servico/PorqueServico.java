package br.com.mdias.pdca.servico;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.CausaRaiz;
import br.com.mdias.pdca.entidade.Porque;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.repositorio.PorqueRepositorio;

@Service
public class PorqueServico extends GenericoCrudServico<Porque, Long> {

	@Autowired
	private PorqueAcaoServico porqueAcaoServico;

	public void excluirPorquePorCausaRaiz(Long idCausaRaiz) {
		Porque filtros = new Porque();
		filtros.setCausaRaiz(new CausaRaiz());
		filtros.getCausaRaiz().setId(idCausaRaiz);
		ArrayList<Porque> itensParaExcluir = (ArrayList<Porque>) this.consultarPor(filtros);

		for (Porque porque : itensParaExcluir) {
			if (porque.getIdPorquePai() == null) {
				this.deletar(porque);
			}
		}
	}

	@Override
	@Transactional
	public void deletar(Porque porque) {
		porque = this.consultarPor(porque.getId());
		if (porque.getFilhos() != null && porque.getFilhos().size() > 0) {
			for (Porque filho : porque.getFilhos()) {
				this.deletar(filho);
			}
		} else if (porque.getPorqueAcoes() != null && porque.getPorqueAcoes().size() > 0) {
			for (PorqueAcao porqueAcao : porque.getPorqueAcoes()) {
				porqueAcaoServico.deletar(porqueAcao);
			}
		}
		porque.setFilhos(null);
		porque.setPorqueAcoes(null);
		porque = super.getRepositorio().save(porque);
		super.getRepositorio().delete(porque);
	}

	public List<Porque> consultarPorCausaRaiz(Long idCausaRaiz) {
		return ((PorqueRepositorio) getRepositorio()).porquesDoPDCA(idCausaRaiz);
	}

}
