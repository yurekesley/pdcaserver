package br.com.mdias.pdca.servico;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.CausaRaiz;
import br.com.mdias.pdca.entidade.Estratificacao;
import br.com.mdias.pdca.entidade.Porque;

@Service
public class CausaRaizServico extends GenericoCrudServico<CausaRaiz, Long> {

	@Autowired
	private PorqueServico porqueServico;

	@Override
	@Transactional
	public void deletar(CausaRaiz causaRaiz) {
		Porque filtro = new Porque();
		filtro.setCausaRaiz(new CausaRaiz());
		filtro.getCausaRaiz().setId(causaRaiz.getId());

		porqueServico.excluirPorquePorCausaRaiz(causaRaiz.getId());

		super.deletar(causaRaiz);
	}

	public void excluirPorEstratificacao(Long idEstratificacao) {

		CausaRaiz filtros = new CausaRaiz();
		filtros.setEstratificacao(new Estratificacao());
		filtros.getEstratificacao().setId(idEstratificacao);

		ArrayList<CausaRaiz> itensParaExcluir = (ArrayList<CausaRaiz>) this.consultarPor(filtros);

		for (CausaRaiz causaRaiz : itensParaExcluir) {
			this.deletar(causaRaiz);
		}

	}

}
