package br.com.mdias.pdca.servico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.Estratificacao;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.dto.EstratificacaoDTO;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import br.com.mdias.pdca.entidade.enums.StatusPDCA;

@Service
public class PDCAServico extends GenericoCrudServico<PDCA, Long> {

	@Autowired
	private EstratificacaoServico estratificacaoServico;

	@Autowired
	private PorqueAcaoServico porqueAcaoServico;

	@Transactional
	public void excluir(PDCA pdca) {
		this.estratificacaoServico.excluirPorPDCA(pdca);
		super.deletar(pdca);
	}

	public BigDecimal calcularPercentualAnalizado(Long idPDCA) {
		BigDecimal totalAnalizado = new BigDecimal(0.0);

		Map<String, Object> filtros = new HashMap<>();
		filtros.put("idPDCA", idPDCA);

		List<EstratificacaoDTO> estratificacoes = this.getConsultaRepositorio()
				.listagem("ESTRATIFICACOES_POR_PDCA_QUE_POSSUEM_ACOES", filtros);

		for (EstratificacaoDTO estratificacao : estratificacoes) {
			totalAnalizado = totalAnalizado.add(this.calcularPorcentagem(estratificacao));
		}
		BigDecimal desconsiderarArredondaemntos = new BigDecimal(100);
		if (totalAnalizado.compareTo(desconsiderarArredondaemntos) >= 0 ) {
			return desconsiderarArredondaemntos;	
		}
		return totalAnalizado;
	}

	private BigDecimal calcularPorcentagem(EstratificacaoDTO dto) {
		List<EstratificacaoDTO> calcularPorcentagem = new ArrayList<EstratificacaoDTO>();
		List<Estratificacao> estratificacoes = buscarEstratificacaoPorHierarquia(dto);
		dto.setPorcentagem(this.calcularPorcentagem(dto, estratificacoes));
		calcularPorcentagem.add(dto);

		if (dto.getPai() != null) {
			List<Estratificacao> pais = this.estratificacaoServico.pais(dto.getId(), null);
			for (Estratificacao pai : pais) {
				EstratificacaoDTO paiDTO = new EstratificacaoDTO();
				paiDTO.convert(pai);
				paiDTO.setPorcentagem(this.calcularPorcentagem(paiDTO, buscarEstratificacaoPorHierarquia(paiDTO)));
				calcularPorcentagem.add(paiDTO);
			}
		}
		BigDecimal procentagemAcumulada = new BigDecimal(1.0);
		for (EstratificacaoDTO porcentagem : calcularPorcentagem) {
			procentagemAcumulada = procentagemAcumulada.multiply(porcentagem.getPorcentagem())
					.divide(new BigDecimal(100.0), 4, BigDecimal.ROUND_CEILING);
		}
		return procentagemAcumulada.multiply(new BigDecimal(100.0));
	}

	private List<Estratificacao> buscarEstratificacaoPorHierarquia(EstratificacaoDTO dto) {
		boolean souFilho = dto.getPai() != null;
		if (souFilho) {
			return this.estratificacaoServico.listarPorPDCAPorPai(dto.getIdPdca(), dto.getPai());
		} else {
			return this.estratificacaoServico.listarPorPDCA(dto.getIdPdca());
		}
	}

	private BigDecimal calcularPorcentagem(EstratificacaoDTO dto, List<Estratificacao> estratificacoes) {
		BigDecimal quantidadeTotal = new BigDecimal(0.0);
		for (Estratificacao estratificacao : estratificacoes) {
			quantidadeTotal = quantidadeTotal.add(new BigDecimal(estratificacao.getQuantidade().doubleValue()));
		}

		BigDecimal valorDTO = new BigDecimal(dto.getQuantidade().doubleValue());
		BigDecimal retorno = valorDTO.multiply(new BigDecimal(100));

		return retorno.divide(quantidadeTotal, 4, BigDecimal.ROUND_CEILING);
	}

	@Transactional
	public PDCA emCadastro(Long id) {
		PDCA pdca = this.consultarPor(id);
		if (pdca.getStatus().equals(StatusPDCA.REPROVADO) || pdca.getStatus().equals(StatusPDCA.AGUARDANDO_APROVACAO)) {
			porqueAcaoServico.alterarStatusDasAcoesPorPDCA(id,StatusAcao.EM_CADASTRO);
			pdca.setStatus(StatusPDCA.EM_CADASTRO);
		}
		return pdca;
	}

	public void atualizarStatusDoPDCA(Long idPDCA, StatusPDCA novoStatus) {
		PDCA pdca = this.consultarPor(idPDCA);
		pdca.setStatus(novoStatus);
		this.salvar(pdca);
	}

}
