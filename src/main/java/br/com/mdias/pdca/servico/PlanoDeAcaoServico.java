package br.com.mdias.pdca.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.consultas.Gerente;
import br.com.mdias.pdca.entidade.dto.ConclusaoPlanoDeAcaoDTO;
import br.com.mdias.pdca.entidade.dto.ConfirmacaoPlanoDeAcao;
import br.com.mdias.pdca.entidade.enums.SimOuNao;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import br.com.mdias.pdca.entidade.enums.StatusPDCA;

@Service
public class PlanoDeAcaoServico {
	
	@Autowired
	private PDCAServico pdcaServico;
	
	@Autowired
	private PorqueAcaoServico porqueAcaoServico;
	
	public void disponibilizar(ConfirmacaoPlanoDeAcao planodeAcao) {

		if (planodeAcao.getIdPDCA() != null && planodeAcao.getGerente() != null) {

			Long idPDCA = planodeAcao.getIdPDCA();
			Gerente gerente = planodeAcao.getGerente();

			PDCA pdca = this.pdcaServico.consultarPor(idPDCA);
	

			this.porqueAcaoServico.alterarStatusDasAcoesPorPDCA(idPDCA, StatusAcao.AGUARDANDO_APROVACAO);
					
			pdca.setStatus(StatusPDCA.AGUARDANDO_APROVACAO);
			pdca.setNomeGerenteAprovador(gerente.getNome());
			pdca.setCodGerenteAprovador(gerente.getCodigo());
			
			this.pdcaServico.salvar(pdca);
		}

	}
	
	@Transactional
	public void concluir(ConclusaoPlanoDeAcaoDTO conclusao) {
		if(possuiAlgumaAcaoReprovada(conclusao)) {
			porqueAcaoServico.reprovar(conclusao.getPorqueAcoes());
			pdcaServico.atualizarStatusDoPDCA(conclusao.getPdca().getId(), StatusPDCA.REPROVADO);
			return ;
		}	
		this.porqueAcaoServico.aprovar(conclusao.getPorqueAcoes());
		porqueAcaoServico.alterarStatusDasAcoesPorPDCA(conclusao.getPdca().getId(), StatusAcao.NAO_INICIADA);
		pdcaServico.atualizarStatusDoPDCA(conclusao.getPdca().getId(), StatusPDCA.EM_EXECUCAO);
	}
	
	private boolean possuiAlgumaAcaoReprovada(ConclusaoPlanoDeAcaoDTO conclusao) {		
		boolean naoFoiAprovado = false;
		for (PorqueAcao porqueAcao : conclusao.getPorqueAcoes()) {
			naoFoiAprovado = porqueAcao.getAprovado().equals(SimOuNao.N);
			if(naoFoiAprovado) {
				break;
			}
		}
		return naoFoiAprovado;
	}
	
}
