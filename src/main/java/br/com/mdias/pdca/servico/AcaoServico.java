package br.com.mdias.pdca.servico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdias.frmk_servico.crud.GenericoCrudServico;
import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.dto.AcaoDTO;

@Service
public class AcaoServico extends GenericoCrudServico<Acao, Long> {

	@Autowired
	private PDCAServico pdcaService;
	
	@Autowired 
	private  PorqueAcaoServico porqueAcaoServico;
	
	public List<Acao> acoesPorPDCA(Long idPdca, Long idProque) {
		Map<String, Object> filtros = new HashMap<>();
		filtros.put("idPDCA", idPdca);

		if (idProque != null) {
			filtros.put("idPorque1", idProque);
			filtros.put("idPorque2", idProque);
		}

		List<Acao> acoes = this.getConsultaRepositorio().listagem("ACOES-POR-PDCA", filtros);
		
		return acoes;
	}
	
	public List<AcaoDTO> acoesExcetoPDCA(Map<String, Object> filtros) {
		List<AcaoDTO> acoes = this.getConsultaRepositorio().listagem("ACOES-EXCETO-PDCA", filtros);

		return acoes;
	}
}
