package br.com.mdias.pdca.servico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.dto.RegistrarResultadoDTO;
import br.com.mdias.pdca.entidade.enums.StatusPDCA;

@Service
public class RegistrarResultadoServico {
	@Autowired
	private PDCAServico pdcaServico;

	
	@Autowired
	private PorqueAcaoServico porqueAcaoServico;
	
	public void confirmar(RegistrarResultadoDTO dto) {
			
		PDCA pdca = this.pdcaServico.consultarPor(dto.getPdca().getId());

			if (pdca != null && dto.getPdca() != null ) {
				
				pdca.setCodGerenteRevisor(dto.getPdca().getCodGerenteRevisor());
				pdca.setNomeGerenteRevisor(dto.getPdca().getNomeGerenteRevisor());
				pdca.setComentario(dto.getPdca().getComentario());		
				pdca.setEfetividadeAtingida(dto.getPdca().getEfetividadeAtingida());
				pdca.setStatus(StatusPDCA.AGUARDANDO_CONCLUSAO);
			
				this.salvarParecerPorqueAcoes(dto.getPorqueAcoes());
				
		}
	}

	private void salvarParecerPorqueAcoes(List<PorqueAcao> porqueAcoes) {
		for (PorqueAcao item : porqueAcoes) {
			
			PorqueAcao porqueAcao = this.porqueAcaoServico.consultarPor(item.getId());
			
			if (porqueAcao != null) {
				this.porqueAcaoServico.salvarParecer(item.getParecer());
				porqueAcao.setParecer(item.getParecer());				
				this.porqueAcaoServico.salvar(porqueAcao);
			}
						
		}
	}

}
