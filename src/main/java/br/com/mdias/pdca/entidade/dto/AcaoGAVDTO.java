package br.com.mdias.pdca.entidade.dto;

import java.util.Date;

import lombok.Data;

public @Data class AcaoGAVDTO {
	
	private Long id;
	private String codEquipe;
	private String acao;
	private String responsavel;
	private Date previsao;
	private Date novaPrevisao;
	private String status;
	
}
