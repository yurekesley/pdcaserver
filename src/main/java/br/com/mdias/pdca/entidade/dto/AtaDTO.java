package br.com.mdias.pdca.entidade.dto;

import java.util.Date;
import java.util.List;

import br.com.mdias.frmk_consulta.modelo.anotacao.ConsultaCustomizada;
import lombok.Data;

public @Data class AtaDTO {
	private Long id;
	private Date data;
	private Date dataReferenciaIndicador;
	
	private String comentario;
	private String codEquipe;
	private String codIndicador;
	
	
	@ConsultaCustomizada(id = "ACOES-GAV-DO-INDICADOR-POR-MES-E-ANO", lista=true, parametros = { "id", "codEquipe", "codIndicador"}) 
	private List<AcaoGAVDTO> acoes;
}
