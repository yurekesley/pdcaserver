package br.com.mdias.pdca.entidade;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "MDB_PDCA_CAUSA_RAIZ")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_CAUSA_RAIZ_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_CAUSA_RAIZ", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
public class CausaRaiz extends EntidadeAuditada {

	@ManyToOne
	@JoinColumn(name = "ID_ESTRATIFICACOES")
	private Estratificacao estratificacao;

	@Column(name = "VAL_DESCRICAO")
	private String descricao;

	@Column(name = "COD_TIPO_CA_RAIZ")
	private Integer tipo;
}
