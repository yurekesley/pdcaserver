package br.com.mdias.pdca.entidade.dto;

import lombok.Data;

public @Data class ReprovacaoDTO {
	private Long id;
	private String oque;
	private String justificativa;
	private String porqueAnalizado;
	private String causaRaiz;
	private String estratificacao;
	private Long idPDCA;
	private Long idEstratificacao;
	private Long idCausaRaiz;
}
