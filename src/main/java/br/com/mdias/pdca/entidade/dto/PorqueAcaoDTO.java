package br.com.mdias.pdca.entidade.dto;

import br.com.mdias.pdca.entidade.enums.StatusAcao;
import lombok.Data;

public @Data class PorqueAcaoDTO {
	private Long idPDCA;
	private StatusAcao statusAcao;
}
