package br.com.mdias.pdca.entidade;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "MDB_PDCA_ESTRATIFICACOES")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_ESTRATIFICACOES_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_ESTRATIFICACOES", unique = true, nullable = false, precision = 22, scale = 0)), })
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@NoArgsConstructor
@Getter
@Setter
public class Estratificacao extends EntidadeAuditada {

	@NotNull(message = "Campo obrigatório")
	@Size(max = 256)
	@Column(name = "DESC_OQUE")
	private String oque;

	@NotNull(message = "Campo obrigatório")
	@Digits(integer = 8, fraction = 2, message = "Valor inválido. Deve possuir no máximo 10 digitos")
	@DecimalMin(value = "0.01")
	@NumberFormat(style = Style.NUMBER)
	@Column(name = "VAL_QUANTIDADE")
	private BigDecimal quantidade;

	@Column(name = "VAL_PRIORIDADE")
	private Integer prioridade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTRATIFICACOES_PAI", insertable = true, updatable = false, nullable = true)
	private Estratificacao pai;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy= "pai")
	private List<Estratificacao> filhos;

	@NotNull(message = "Campo obrigatório")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PDCAS", insertable = true, updatable = false, nullable = false)
	private PDCA pdca;

	@Formula("( SELECT count(es.ID_ESTRATIFICACOES) FROM MDB_PDCA_ESTRATIFICACOES  es where es.id_estratificacoes_pai = ID_ESTRATIFICACOES )")
	private Integer qtdFilhos;

	@Formula("( select count(cr.id_causa_raiz) from MDB_PDCA_CAUSA_RAIZ cr where cr.id_estratificacoes = ID_ESTRATIFICACOES )")
	private Integer qtdCausaRaiz;

	@Override
	public boolean equals(Object obj) {
		Estratificacao other = (Estratificacao) obj;

		if (getClass() != obj.getClass())
			return false;
		if (getId() == null || other.getId() == null)
			return false;
		if (!getId().equals(other.getId()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

}
