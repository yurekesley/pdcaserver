package br.com.mdias.pdca.entidade.dto;

import java.util.List;

import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.PorqueAcao;
import lombok.Data;

public @Data class RegistrarResultadoDTO {
	
	private PDCA pdca;
	private List<PorqueAcao> porqueAcoes;
	
}
