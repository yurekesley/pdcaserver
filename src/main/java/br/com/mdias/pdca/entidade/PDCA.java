package br.com.mdias.pdca.entidade;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.mdias.frmk_consulta.modelo.anotacao.ConsultaCustomizada;
import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.EfetividadeAtingida;
import br.com.mdias.pdca.entidade.enums.StatusPDCA;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "MDB_PDCA_PDCAS")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_PDCAS_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_PDCAS", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PDCA extends EntidadeAuditada {

	@NotNull(message = "Campo obrigatório")
	@Column(name = "COD_EQUIPE", nullable = false, length = 50)
	private String codEquipe;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "DESC_EQUIPE", nullable = false, length = 255)
	private String descEquipe;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "COD_INDICADOR", nullable = false, length = 50)
	private String codIndicador;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "DESC_INDICADOR", nullable = false, length = 50)
	private String descIndicador;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "VAL_MES", nullable = false, length = 50)
	private Integer mes;

	@NotNull(message = "Campo obrigatório")
	@Column(name = "VAL_ANO", nullable = false, length = 50)
	private Integer ano;

	@NotNull(message = "Campo obrigatório")
	@Enumerated(EnumType.STRING)
	@Column(name = "ST_PDCA", nullable = false, length = 50)
	private StatusPDCA status;

	@Column(name = "DESC_COMENTARIO", nullable = true, length = 500)
	private String comentario;

	@Enumerated(EnumType.STRING)
	@Column(name = "FLAG_EFEITO_POSITIVO")
	private EfetividadeAtingida efetividadeAtingida;
	
	
	@Formula("ID_PDCAS")
	private Long idPDCA;
	
	@Transient
	@ConsultaCustomizada(id = "QTD_ACOES_PDCA", parametros = { "idPDCA" })
	private Integer qtdAcoes;

	@Transient
	@ConsultaCustomizada(id = "QTD_ACOES_ATRAZADAS_PDCA", parametros = { "idPDCA" })
	private Integer acoesAtrazadas;

	@Transient
	@ConsultaCustomizada(id = "QTD-REPROVACOES-POR-PDCA", parametros = { "idPDCA" })
	private Integer qtdReprovacoes;
	
	@Column(name = "DESC_GERENTE_APROVADOR", nullable = false, length = 255)
	private String nomeGerenteAprovador;

	@Column(name = "COD_GERENTE_APROVADOR", nullable = false, length = 255)
	private String codGerenteAprovador;

	@Column(name = "COD_GERENTE_REVISOR", length = 255)
	private String codGerenteRevisor;

	@Column(name = "DESC_GERENTE_REVISOR", length = 255)
	private String nomeGerenteRevisor;

}
