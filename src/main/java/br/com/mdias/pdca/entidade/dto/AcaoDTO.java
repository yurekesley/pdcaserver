package br.com.mdias.pdca.entidade.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.mdias.pdca.entidade.Acao;
import lombok.Data;

public @Data class AcaoDTO {

	private Long id;
	private Long idPorqueAcao;
	private String criadoPor;
	private String atualizadoPor;
	private Date dataCriacao;
	private Date dataAtualizacao;

	private String oque;
	private String quem;
	private String porque;
	private String onde;
	private String como;
	private Date quando;

	private Date conclusao;
	private String statusAtraso;
	private String statusAcao;
	private String efeitoPositivo;
	private String parecer;
	private String pdca;
	private Integer totalDeMensagens;
	private String perguntaPorqueAnalizado;

	public static List<Acao> transform(List<AcaoDTO> dtos) {

		List<Acao> acoes = new ArrayList<Acao>();

		for (AcaoDTO dto : dtos) {

			Acao acao = new Acao();

			acao.setId(dto.id);
			acao.setCriadoPor(dto.getCriadoPor());
			acao.setAtualizadoPor(dto.getAtualizadoPor());
			acao.setDataCriacao(dto.getDataCriacao());
			acao.setDataAtualizacao(dto.getDataAtualizacao());
			acao.setOque(dto.getOque());
			acao.setQuem(dto.getQuem());
			acao.setPorque(dto.getPorque());
			acao.setOnde(dto.getOnde());
			acao.setComo(dto.getComo());
			acao.setQuando(dto.getQuando());

			acoes.add(acao);
		}

		return acoes;
	}

}
