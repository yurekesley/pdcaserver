package br.com.mdias.pdca.entidade;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import br.com.mdias.pdca.entidade.enums.StatusAtrasoAcao;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MDB_PDCA_ACOES")
@SuppressWarnings("serial")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_ACOES_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_ACOES", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Acao extends EntidadeAuditada {

	@Column(name = "DESC_OQUE")
	private String oque;

	@Column(name = "DESC_QUEM")
	private String quem;

	@Column(name = "DESC_PORQUE")
	private String porque;

	@Column(name = "DESC_ONDE")
	private String onde;

	@Column(name = "DESC_COMO")
	private String como;

	@Column(name = "DATA_QUANDO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date quando;

	@Enumerated(EnumType.STRING)
	@Column(name = "ST_ATRASO", nullable = false, length = 1)
	private StatusAtrasoAcao statusAtraso;

	@Enumerated(EnumType.STRING)
	@Column(name = "ST_ACAO", nullable = false, length = 50)
	private StatusAcao statusAcao;

	@Column(name = "DATA_CONCLUSAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date conclusao;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PDCA_ORIGEM")
	private PDCA origem;

}
