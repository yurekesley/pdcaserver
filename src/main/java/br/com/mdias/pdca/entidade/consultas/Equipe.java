package br.com.mdias.pdca.entidade.consultas;

import lombok.Data;

public @Data class Equipe {

	private String id;
	private String nome;
	private String descricao;

}