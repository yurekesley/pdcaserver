package br.com.mdias.pdca.entidade.consultas;

import lombok.Data;

public @Data class Indicador {

	private String versao;
	private String codIndicador;
	private String descricao;
	private String direcao;
	private String status;
	private String codEquipe;
	private String descricaoEquipe;
	
	
}
