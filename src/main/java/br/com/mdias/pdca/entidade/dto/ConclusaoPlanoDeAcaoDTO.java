package br.com.mdias.pdca.entidade.dto;

import java.util.List;

import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.entidade.PorqueAcao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public  class ConclusaoPlanoDeAcaoDTO {
	private PDCA pdca;
	private List<PorqueAcao> porqueAcoes;
	private List<Acao> acoes;	
}
