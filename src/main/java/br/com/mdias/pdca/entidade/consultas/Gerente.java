package br.com.mdias.pdca.entidade.consultas;

import lombok.Data;

public @Data class Gerente {
	private String codigo;
	private String nome;
}
