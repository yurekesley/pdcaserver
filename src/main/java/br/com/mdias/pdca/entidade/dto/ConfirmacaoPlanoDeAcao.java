package br.com.mdias.pdca.entidade.dto;

import br.com.mdias.pdca.entidade.consultas.Gerente;
import lombok.Data;

public @Data class ConfirmacaoPlanoDeAcao {
	
	private Long idPDCA;
	private Gerente gerente;
}
