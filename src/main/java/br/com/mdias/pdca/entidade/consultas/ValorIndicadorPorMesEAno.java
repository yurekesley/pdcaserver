package br.com.mdias.pdca.entidade.consultas;

import lombok.Data;

public @Data class ValorIndicadorPorMesEAno {
		private String codIndicador;
		private String nome;
		private String versao;
		private String direcao;
		private String tipo; // META OU REALIZADO
		private Double valor;
		private String mes;
		private String ano;
}
