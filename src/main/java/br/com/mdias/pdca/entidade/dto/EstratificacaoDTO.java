package br.com.mdias.pdca.entidade.dto;

import java.math.BigDecimal;
import java.util.List;

import br.com.mdias.frmk_consulta.modelo.anotacao.ConsultaCustomizada;
import br.com.mdias.pdca.entidade.Estratificacao;
import lombok.Data;

public @Data class EstratificacaoDTO  {
	
	private Long id;
	private String oque;
	private BigDecimal quantidade;
	private Integer prioridade;
	private Long idPdca;
	private Long pai;
	private BigDecimal porcentagem;
	
	@ConsultaCustomizada(id= "ESTRATIFICACOES_POR_ESTRATIFICACAO", camposMinimos = {"id", "idPdca"}, parametros = {"id", "idPdca"}, lista = true)
	private List<EstratificacaoDTO> filhos;
	
	@ConsultaCustomizada(id= "ESTRATIFICACAO_POSSUI_CAUSA_RAIZ", camposMinimos = {"id"}, parametros = {"id"} )
	private Integer hasCausaRaiz;
	
	public void convert(Estratificacao estratificacao) {
		this.id = estratificacao.getId();
		this.oque = estratificacao.getOque();
		this.quantidade = estratificacao.getQuantidade();
		this.prioridade = estratificacao.getPrioridade();
		this.idPdca = estratificacao.getPdca() != null ? estratificacao.getPdca().getId() : null;
		this.pai = estratificacao.getPai() != null ? estratificacao.getPai().getId() : null;
	}

}
