package br.com.mdias.pdca.entidade;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MDB_PDCA_PQ_ACOES_REPROVACAO")
@SuppressWarnings("serial")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_PQ_ACOES_REPROVACAO_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_PQ_ACOES_REPROVACAO", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PorqueAcaoReprovacao extends EntidadeAuditada{
		
	@NotNull(message = "Campo obrigatório")
	@Size(max = 100)
	@Column(name = "DESC_JUSTIFICATIVA")
	private String justificativa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PORQUE_ACOES")
	@JsonBackReference
	private PorqueAcao porqueAcao;
	
	@Transient
	@JsonProperty("acao")
	public Acao acao() {
		return porqueAcao.getAcao();
	}
	


}
