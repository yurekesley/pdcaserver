package br.com.mdias.pdca.entidade.enums;

public enum StatusAcao {
	EM_CADASTRO ,
	AGUARDANDO_APROVACAO, 
	REPROVADA,
	APROVADA,
	NAO_INICIADA, 
	EM_EXECUCAO, 
	CONCLUIDA
}
