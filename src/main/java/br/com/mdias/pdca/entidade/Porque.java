package br.com.mdias.pdca.entidade;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.SimOuNao;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "MDB_PDCA_PORQUES")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_PORQUES_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_PORQUES", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
public  class Porque extends EntidadeAuditada {

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "ID_PORQUES_PAI", insertable = true, updatable = false, nullable = true)
	private Set<Porque> filhos;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, mappedBy= "porque")
	@JsonManagedReference
	private Set<PorqueAcao> porqueAcoes;

	@NotNull
	@Size(max = 256)
	@Column(name = "DESC_PERGUNTA")
	private String pergunta;

	@NotNull
	@Size(max = 256)
	@Column(name = "DESC_RESPOSTA")
	private String resposta;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "ST_ATIVO")
	private SimOuNao ativo;

	@Column(name = "ID_PORQUES_PAI")
	private Long idPorquePai;

	@ManyToOne
	@JoinColumn(name = "ID_CAUSA_RAIZ")
	private CausaRaiz causaRaiz;

}
