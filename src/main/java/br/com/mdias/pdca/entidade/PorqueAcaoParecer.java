package br.com.mdias.pdca.entidade;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.EfetividadeAtingida;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MDB_PDCA_PQ_ACOES_PARECER")
@SuppressWarnings("serial")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_PQ_ACOES_PARECER_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_PQ_ACOES_PARECER", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PorqueAcaoParecer extends EntidadeAuditada {
	
	@Enumerated(EnumType.STRING)
	@Column(name = "FLAG_EFEITO_POSITIVO")
	private EfetividadeAtingida efeitoPositivo;
	
	@NotNull(message = "Campo obrigatório")
	@Size(max = 255)
	@Column(name = "DESC_PARECER")
	private String descricao;
	

}
