package br.com.mdias.pdca.entidade;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.StatusAcao;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MDB_PDCA_COMENTARIO")
@SuppressWarnings("serial")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_COMENTARIO_ACAO_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_COMENTARIO", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
public class Comentario extends EntidadeAuditada {

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "ID_ACOES")
	private Acao acao;

	@Enumerated(EnumType.STRING)
	@Column(name = "ST_ACAO", nullable = false, length = 50)
	private StatusAcao statusAcao;

	@Column(name = "DESC_COMENTARIO")
	private String comentario;
}
