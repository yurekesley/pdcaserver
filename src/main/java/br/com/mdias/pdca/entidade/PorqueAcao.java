package br.com.mdias.pdca.entidade;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.mdias.frmk_entidade.modelo.EntidadeAuditada;
import br.com.mdias.frmk_utilitario.util.ConexaoUtil;
import br.com.mdias.pdca.entidade.enums.SimOuNao;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MDB_PDCA_PORQUE_ACOES")
@SuppressWarnings("serial")
@SequenceGenerator(name = ConexaoUtil.GERADOR_SEQUENCIA, sequenceName = "MDB_PDCA_PORQUE_ACOES_S", allocationSize = 1)
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "ID_PORQUE_ACOES", unique = true, nullable = false, precision = 22, scale = 0)), })
@Getter
@Setter
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PorqueAcao extends EntidadeAuditada {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PORQUES")
	@JsonBackReference
	private Porque porque;
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ACOES")
	private Acao acao;
			
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "porqueAcao")
	@JsonManagedReference
	private List<PorqueAcaoReprovacao> reprovacoes;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "FLAG_APROVADO")
	private SimOuNao aprovado;

	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PQ_ACOES_PARECER")
	private PorqueAcaoParecer parecer;
	
	@Transient
	@JsonProperty("porqueDTO")
	public Porque porqueDTO() {
		if(porque != null) {
			Porque aux = new Porque();
			aux.setId(porque.getId());
			aux.setCausaRaiz(porque.getCausaRaiz());
			porque.setFilhos(null);
			return aux;
		}
		
		return null;
	}
	
	
	
}
