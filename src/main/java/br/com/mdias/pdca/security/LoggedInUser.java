package br.com.mdias.pdca.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class LoggedInUser {

	/**
	 * Retorna nome do usuário corrente
	 */
	public String getUsername() {
		UserDetails user = currentUser();
		return user.getUsername();
	}

	private UserDetails currentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null)
			throw new IllegalStateException("Usuário logado não encontrado");
		
		Object principal = auth.getPrincipal();
		if (principal instanceof UserDetails) {
			UserDetails user = (UserDetails) principal;
			return user; 
		}
		
		return (UserDetails) auth.getDetails();
	}
}
