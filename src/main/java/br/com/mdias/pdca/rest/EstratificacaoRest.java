package br.com.mdias.pdca.rest;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.Estratificacao;
import br.com.mdias.pdca.entidade.dto.EstratificacaoDTO;
import br.com.mdias.pdca.servico.EstratificacaoServico;

@RestController
@RequestMapping(value = "/estratificacao/", produces = RestUtil.JSON_PRODUCE)
public class EstratificacaoRest {

	@Autowired
	public EstratificacaoServico servico;

	@DeleteMapping(value = "excluir/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.DELETAR })
	public JsonResult excluir(@PathVariable(value = "id") Long id) {
		Estratificacao estratificacao = servico.consultarPor(id);
		return Json.success().withMessage("Deletado com sucesso").withData(servico.excluir(estratificacao)).build();
	}

	@DeleteMapping(value = "{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.DELETAR })
	public JsonResult excluirTodos(@PathVariable(value = "id") Long id) {
		servico.excluirTodos(id);
		return Json.success().withMessage("Deletado com sucesso").withData(null).build();
	}

	@PostMapping(value = "salvar")
	@AuditoriaMdias(permissoes = { PermissaoMDias.INSERIR })
	public JsonResult salvar(@Valid @RequestBody Estratificacao estratificacao) {
		return Json.success().withMessage("Salvo com sucesso").withData(servico.adicionar(estratificacao)).build();
	}

	@PutMapping(value = "atualizar")
	@AuditoriaMdias(permissoes = { PermissaoMDias.ATUALIZAR })
	public JsonResult atualizar(@Valid @RequestBody Estratificacao estratificacao) {
		return Json.success().withMessage("Atualizado com sucesso").withData(servico.atualizar(estratificacao)).build();
	}

	@PutMapping(value = "prioridade/aumentar")
	@AuditoriaMdias(permissoes = { PermissaoMDias.ATUALIZAR })
	public JsonResult aumentarPrioridade(@RequestBody Estratificacao estratificacao) {
		return Json.success().withMessage("Atualizado com sucesso").withData(servico.aumentarPrioridade(estratificacao))
				.build();
	}

	@PutMapping(value = "prioridade/diminuir")
	@AuditoriaMdias(permissoes = { PermissaoMDias.ATUALIZAR })
	public JsonResult diminuirPrioridade(@RequestBody Estratificacao estratificacao) {
		return Json.success().withMessage("Atualizado com sucesso").withData(servico.diminuirPrioridade(estratificacao))
				.build();
	}

	@GetMapping(value = "pdca/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult estratificacoesDoPDCA(@PathVariable(value = "id") Long idPDCA) {
		ArrayList<Estratificacao> estratificacoes = (ArrayList<Estratificacao>) servico.listarPorPDCA(idPDCA);
		servico.ordenarPorPrioridade(estratificacoes);
		return Json.success().withMessage("").withData(estratificacoes).build();
	}

	@GetMapping(value = "visualizar/pdca/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult visualizarEstratificacao(@PathVariable(value = "id") Long idPDCA) {
		ArrayList<EstratificacaoDTO> estratificacoes = (ArrayList<EstratificacaoDTO>) servico.visualizarDTO(idPDCA);

		return Json.success().withMessage("").withData(estratificacoes).build();
	}

	@GetMapping(value = "{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult buscar(@PathVariable(value = "id") Long id) {
		return Json.success().withMessage("").withData(servico.consultarPor(id)).build();
	}

	@GetMapping(value = "filhos/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult filhos(@PathVariable(value = "id") Long id) {
		return Json.success().withMessage("").withData(servico.filhos(id)).build();
	}

	@GetMapping(value = "pais/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult pais(@PathVariable(value = "id") Long id) {
		return Json.success().withMessage("").withData(servico.pais(id, null)).build();
	}

}
