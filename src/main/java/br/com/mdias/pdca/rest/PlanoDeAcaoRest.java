package br.com.mdias.pdca.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.dto.ConclusaoPlanoDeAcaoDTO;
import br.com.mdias.pdca.entidade.dto.ConfirmacaoPlanoDeAcao;
import br.com.mdias.pdca.servico.PlanoDeAcaoServico;

@RestController
@RequestMapping(value = "/plano-de-acao", produces = RestUtil.JSON_PRODUCE)
public class PlanoDeAcaoRest {
	
	@Autowired
	private PlanoDeAcaoServico planoDeAcaoServico;
	
	@PostMapping(value = "disponibilizar")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult disponibilizar(@RequestBody ConfirmacaoPlanoDeAcao planoDeAcao) {
		this.planoDeAcaoServico.disponibilizar(planoDeAcao);
		return Json.success().withMessage("Plano de Ação disponibilizado para aprovação").withData(null)
				.build();
	}
	
	
	@PostMapping(value = "concluir")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult concluir(@RequestBody ConclusaoPlanoDeAcaoDTO conclusao) {
		this.planoDeAcaoServico.concluir(conclusao);
		return Json.success().withData(null).build();
	}
	

}
