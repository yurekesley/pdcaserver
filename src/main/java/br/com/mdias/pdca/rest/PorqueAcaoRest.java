package br.com.mdias.pdca.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.dto.PorqueAcaoDTO;
import br.com.mdias.pdca.servico.PorqueAcaoServico;

@RestController
@RequestMapping(value = "/porque-acao/", produces = RestUtil.JSON_PRODUCE)
public class PorqueAcaoRest extends GenericoCrudRestController<PorqueAcao, Long> {
	
	@Autowired
	PorqueAcaoServico servico;
	
	@GetMapping(value="pdca/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult listarPorPDCA(@PathVariable(value="id") Long idPDCA) {
		return Json.success()
				.withMessage("")
				.withData(
						servico.listarPorPDCA(idPDCA)
				)
				.build();
	}
	
	
	@PostMapping(value="/status-acao/pdca")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult acoesPorPDCAStatusAcao(@RequestBody PorqueAcaoDTO porqueAcaoDTO) {
		return Json.success()
				.withMessage("")
				.withData(
						servico.acoesPorPDCAStatusAcao(porqueAcaoDTO)
				)
				.build();
	}
	
	@PostMapping(value="/vincular")
	@AuditoriaMdias(permissoes = { PermissaoMDias.INSERIR })
	public JsonResult vincular(@Valid @RequestBody PorqueAcao porqueAcao) {
		return Json.success()
				.withMessage(i18n("msg.crud.salvar.sucesso"))
				.withData(servico.vincular(porqueAcao))
				.build();
	}


}
