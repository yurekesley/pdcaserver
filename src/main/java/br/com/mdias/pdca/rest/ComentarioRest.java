package br.com.mdias.pdca.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.Comentario;

@RestController
@RequestMapping(value = "/comentario", produces = RestUtil.JSON_PRODUCE)
public class ComentarioRest extends GenericoCrudRestController<Comentario, Long> {

}
