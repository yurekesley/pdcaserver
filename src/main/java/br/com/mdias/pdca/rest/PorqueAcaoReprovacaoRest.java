package br.com.mdias.pdca.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.servico.PorqueAcaoReprovacaoServico;

@RestController
@RequestMapping(value = "/reprovacao/", produces = RestUtil.JSON_PRODUCE)
public class PorqueAcaoReprovacaoRest {
	
	@Autowired
	public PorqueAcaoReprovacaoServico servico;
	
	@GetMapping(value="pdca/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult porPDCA(@PathVariable(value="id") Long idPDCA) {
		return Json.success()
				.withMessage("")
				.withData(
						servico.porPDCA(idPDCA)
				)
				.build();
	}

}
