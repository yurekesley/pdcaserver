package br.com.mdias.pdca.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.Acao;
import br.com.mdias.pdca.entidade.dto.AcaoDTO;
import br.com.mdias.pdca.entidade.dto.ConfirmacaoPlanoDeAcao;
import br.com.mdias.pdca.servico.AcaoServico;

@RestController
@RequestMapping(value = "/acao", produces = RestUtil.JSON_PRODUCE)
public class AcaoRest extends GenericoCrudRestController<Acao, Long> {

	@GetMapping(value = "/consultar/pdca/{idPdca}/{idPorque}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult consultarPor(@PathVariable(value = "idPdca") Long idPdca,
			@PathVariable(value = "idPorque") Long idPorque) {
		List<Acao> acoes = ((AcaoServico) this.getServico()).acoesPorPDCA(idPdca, idPorque);
		return Json.success().withData(acoes).build();
	}

	@PostMapping(value = "/consultar/exceto/pdca")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult consultarPor(@RequestBody Map<String, Object> map) {
		List<AcaoDTO> acoes = ((AcaoServico) this.getServico()).acoesExcetoPDCA(map);
		return Json.success().withData(acoes).build();
	}	

}
