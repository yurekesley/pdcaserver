package br.com.mdias.pdca.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.Porque;
import br.com.mdias.pdca.servico.PorqueServico;

@RestController
@RequestMapping(value = "/porque", produces = RestUtil.JSON_PRODUCE)
public class PorqueRest extends GenericoCrudRestController<Porque, Long> {

	@DeleteMapping(value = "/deletar/causa-raiz/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.DELETAR })
	public JsonResult deletarTodosPorEstratificacao(@PathVariable(value = "id") Long idCausaRaiz) {
		((PorqueServico) this.getServico()).excluirPorquePorCausaRaiz(idCausaRaiz);
		return Json.success().withMessage("Deletado com sucesso").withData(null).build();
	}

	@GetMapping(value = "/consultar/causa-raiz/{idCausaRaiz}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult consultarPorquePorCausaRaiz(@PathVariable(value = "idCausaRaiz") Long idCausaRaiz) {
		List<Porque> porques = ((PorqueServico) this.getServico()).consultarPorCausaRaiz(idCausaRaiz);
		return Json.success().withData(porques).build();
	}
}
