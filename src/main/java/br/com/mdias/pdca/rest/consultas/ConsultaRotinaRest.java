package br.com.mdias.pdca.rest.consultas;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.rest.GenericoConsultaRest;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.repositorio.consultas.ConsultaRotinaRepositorio;

@RestController
@RequestMapping(value = "/consulta/rotina", produces = { RestUtil.JSON_PRODUCE })
public class ConsultaRotinaRest extends GenericoConsultaRest<ConsultaRotinaRepositorio> {

}
