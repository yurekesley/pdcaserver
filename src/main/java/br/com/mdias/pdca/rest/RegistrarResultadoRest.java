package br.com.mdias.pdca.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.dto.RegistrarResultadoDTO;
import br.com.mdias.pdca.servico.RegistrarResultadoServico;

@RestController
@RequestMapping(value = "/registrar-resultado", produces = RestUtil.JSON_PRODUCE)
public class RegistrarResultadoRest {
	
	@Autowired
	private RegistrarResultadoServico service;
	
	@PostMapping(value="/confirmar")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult salvar(@Valid @RequestBody RegistrarResultadoDTO dto) {
		
		this.service.confirmar(dto);
		
		return Json.success()
				.withMessage("PDCA Disponibilizado para conclusão")
				.withData(null)
				.build();
	}

}
