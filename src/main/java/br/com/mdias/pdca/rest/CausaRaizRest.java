package br.com.mdias.pdca.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.CausaRaiz;
import br.com.mdias.pdca.entidade.Estratificacao;
import br.com.mdias.pdca.servico.CausaRaizServico;

@RestController
@RequestMapping(value = "/causa-raiz", produces = RestUtil.JSON_PRODUCE)
public class CausaRaizRest extends GenericoCrudRestController<CausaRaiz, Long> {

	@GetMapping(value = "/consultar/estratificacao/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult consultarCausasRaizesDaEstratificacao(@PathVariable(value = "id") Long idEstratificacao) {
		CausaRaiz filtro = new CausaRaiz();
		filtro.setEstratificacao(new Estratificacao());
		filtro.getEstratificacao().setId(idEstratificacao);
		return consultarPor(filtro);
	}

	@DeleteMapping(value = "/deletar/estratificacao/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.DELETAR })
	public JsonResult deletarTodosPorEstratificacao(@PathVariable(value = "id") Long idEstratificacao) {
		((CausaRaizServico) getServico()).excluirPorEstratificacao(idEstratificacao);
		return Json.success().withMessage("Deletado com sucesso").withData(null).build();
	}

}
