package br.com.mdias.pdca.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdias.frmk_rest.core.GenericoCrudRestController;
import br.com.mdias.frmk_rest.core.response.jsend.Json;
import br.com.mdias.frmk_rest.core.response.jsend.JsonResult;
import br.com.mdias.frmk_utilitario.anotacao.AuditoriaMdias;
import br.com.mdias.frmk_utilitario.enums.PermissaoMDias;
import br.com.mdias.frmk_utilitario.util.RestUtil;
import br.com.mdias.pdca.entidade.PDCA;
import br.com.mdias.pdca.servico.PDCAServico;

@RestController
@RequestMapping(value = "/pdca", produces = RestUtil.JSON_PRODUCE)
public class PDCARest extends GenericoCrudRestController<PDCA, Long> {
	
	@Autowired
	private PDCAServico pdcaServico;
	
	@DeleteMapping(value="/excluir/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.DELETAR })
	public JsonResult excluir(@PathVariable(value="id") Long id) {
		PDCA pdca = pdcaServico.consultarPor(id);
		this.pdcaServico.excluir(pdca);
		return Json.success()
				.withMessage("Deletado com sucesso")
				.withData(null)
				.build();
	}
	
	@GetMapping(value="/percentual-analizado/{id}")
	@AuditoriaMdias(permissoes = { PermissaoMDias.CONSULTAR })
	public JsonResult calcularPercentualAnalizado(@PathVariable(value="id") Long id) {
		return Json.success()
				.withData(this.pdcaServico.calcularPercentualAnalizado(id))
				.build();
	}

	@PutMapping(value="/em-cadastro")
	@AuditoriaMdias(permissoes = { PermissaoMDias.ATUALIZAR })
	public JsonResult diminuirPrioridade(@RequestBody PDCA pdca) {
		return Json.success()
				.withMessage("Atualizado com sucesso")
				.withData(
						this.pdcaServico.emCadastro(pdca.getId())
				)
				.build();
	}

}
