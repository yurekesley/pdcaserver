package br.com.mdias.pdca;

import java.util.Locale;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import br.com.mdias.frmk_consulta.init.FrmkConsultaApplication;
import br.com.mdias.frmk_entidade.init.FrmkEntidadeApplication;
import br.com.mdias.frmk_rest.init.FrmkRestApplication;
import br.com.mdias.frmk_seguranca.init.FrmkSegurancaApplication;
import br.com.mdias.frmk_servico.init.FrmkServicoApplication;
import br.com.mdias.frmk_utilitario.init.FrmkUtilitarioApplication;

@SpringBootApplication(scanBasePackages = "br.com.mdias.pdca")
@Import({	FrmkUtilitarioApplication.class, 
			FrmkEntidadeApplication.class, 
			FrmkSegurancaApplication.class,
			FrmkConsultaApplication.class, 
			FrmkServicoApplication.class, 
			FrmkRestApplication.class 
		})
public class PDCAApplicationContext implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PDCAApplicationContext.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Locale.setDefault(new Locale("pt", "BR"));
	}
}
