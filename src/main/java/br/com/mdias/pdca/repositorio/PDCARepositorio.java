package br.com.mdias.pdca.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mdias.pdca.entidade.PDCA;

public interface PDCARepositorio extends JpaRepository<PDCA, Long> {

}
