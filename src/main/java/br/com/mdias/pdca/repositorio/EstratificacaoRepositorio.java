package br.com.mdias.pdca.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mdias.pdca.entidade.Estratificacao;

@Repository
public interface EstratificacaoRepositorio extends JpaRepository<Estratificacao, Long> {

	@Query("FROM Estratificacao estratificacao where estratificacao.pdca.id = :idPDCA AND estratificacao.pai is null")
	List<Estratificacao> primeirasEstratificacoes(@Param("idPDCA") Long idPDCA);

	@Query("FROM Estratificacao estratificacao where estratificacao.pdca.id = :idPDCA AND estratificacao.pai.id = :idPai")
	List<Estratificacao> listarPorPai(@Param("idPDCA") Long idPDCA, @Param("idPai") Long idPai);

}
