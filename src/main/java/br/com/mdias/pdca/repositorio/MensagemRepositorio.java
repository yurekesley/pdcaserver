package br.com.mdias.pdca.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mdias.pdca.entidade.Comentario;

@Repository
public interface MensagemRepositorio extends JpaRepository<Comentario, Long> {
}
