package br.com.mdias.pdca.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mdias.pdca.entidade.PorqueAcao;
import br.com.mdias.pdca.entidade.enums.StatusAcao;

@Repository
public interface PorqueAcoesRepositorio extends JpaRepository<PorqueAcao, Long> {
	@Query("FROM PorqueAcao porqueAcao where porqueAcao.porque.causaRaiz.estratificacao.pdca.id = :idPDCA")
	List<PorqueAcao> listaPorqueAcaoPorPDCA(@Param("idPDCA") Long idPDCA);
	
	@Query("FROM PorqueAcao porqueAcao where "
			+ "porqueAcao.porque.causaRaiz.estratificacao.pdca.id = :idPDCA"
			+ " AND porqueAcao.acao.statusAcao = :statusAcao")
	List<PorqueAcao> acoesPorPDCAStatusAcao(@Param("idPDCA") Long idPDCA, @Param("statusAcao") StatusAcao statusAcao);
		
	
}
