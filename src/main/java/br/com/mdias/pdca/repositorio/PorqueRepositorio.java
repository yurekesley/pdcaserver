package br.com.mdias.pdca.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.mdias.pdca.entidade.Porque;

public interface PorqueRepositorio extends JpaRepository<Porque, Long> {

	@Query("FROM Porque pq where pq.causaRaiz.id = :idCausaRaiz AND pq.idPorquePai is null ORDER BY pq.id DESC")
	List<Porque> porquesDoPDCA(@Param("idCausaRaiz") Long idCausaRaiz);

}
