package br.com.mdias.pdca.repositorio.consultas;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import br.com.mdias.frmk_consulta.modelo.anotacao.ConsultaBuilder;
import br.com.mdias.frmk_consulta.repositorio.ConsultaRepositorioGenerico;
import br.com.mdias.frmk_utilitario.util.ConfiguracaoUtil;

@Repository("consultaEBSRepositorio")
public class ConsultaEBSRepositorio extends ConsultaRepositorioGenerico {
		
		public ConsultaEBSRepositorio(ApplicationContext applicationContext) {
			super(ConfiguracaoUtil.getPropriedade("config.conexao.consultas.ebs.xml"),
					ConfiguracaoUtil.getPropriedade("config.conexao.consultas.ebs.ds"),
					new ConsultaBuilder(applicationContext));
		}
}


