package br.com.mdias.pdca.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mdias.pdca.entidade.PorqueAcaoReprovacao;

@Repository
public interface PorqueAcaoReprovacaoRepositorio extends JpaRepository<PorqueAcaoReprovacao, Long> {

}
