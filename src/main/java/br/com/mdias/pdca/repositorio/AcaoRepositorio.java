package br.com.mdias.pdca.repositorio;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mdias.pdca.entidade.Acao;

@Repository
public interface AcaoRepositorio extends JpaRepository<Acao, Long> {
	
	@Transactional
	@Modifying
	@Query("UPDATE Acao acao SET acao.statusAtraso = 'S' WHERE trunc(:date) > trunc(acao.quando) AND acao.conclusao is null")
	int atualizarAtrasos(@Param("date") Date date);
}
