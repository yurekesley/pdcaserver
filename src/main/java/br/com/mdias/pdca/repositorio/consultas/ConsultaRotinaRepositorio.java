package br.com.mdias.pdca.repositorio.consultas;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import br.com.mdias.frmk_consulta.modelo.anotacao.ConsultaBuilder;
import br.com.mdias.frmk_consulta.repositorio.ConsultaRepositorioGenerico;
import br.com.mdias.frmk_utilitario.util.ConfiguracaoUtil;

@Repository("consultaRotinaRepositorio")
public class ConsultaRotinaRepositorio extends ConsultaRepositorioGenerico {
	
	public ConsultaRotinaRepositorio(ApplicationContext applicationContext) {
		super(ConfiguracaoUtil.getPropriedade("config.conexao.consultas.rotina.xml"),
				ConfiguracaoUtil.getPropriedade("config.conexao.consultas.rotina.ds"),
				new ConsultaBuilder(applicationContext));
	}
}
